import React, { useState } from "react";

import { CardFrontProps, FrontCard } from "./FrontCard/FrontCard";
import { BackwardCard, CardBackwardProps } from "./BackwardCard/BackwardCard";
import * as S from "./CreditCard.styles";

interface CreditCard extends CardFrontProps, CardBackwardProps {}

interface CreditCardProps {
  creditCard: CreditCard;
}

export const CreditCard: React.FC<CreditCardProps> = ({ creditCard }) => {
  const [cardInformations, setCardInformations] =
    useState<CreditCard>(creditCard);
  const [focused, setFocused] = useState<"front" | "backwards">("front");

  return (
    <S.CardContainer>
      <FrontCard
        isFocused={focused === "front"}
        onClick={() => setFocused("front")}
        frontalCard={creditCard}
      />

      {creditCard?.cvv && (
        <BackwardCard
          isFocused={focused === "backwards"}
          onClick={() => setFocused("backwards")}
          backwardCard={creditCard}
        />
      )}
    </S.CardContainer>
  );
};
