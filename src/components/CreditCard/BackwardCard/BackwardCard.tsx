import React, { useState, useEffect } from "react";

import * as S from "./BackwardCard.styles";

export interface CardBackwardProps {
  cvv?: string;
}

interface BackwardCardProps {
  backwardCard: CardBackwardProps;
  isFocused?: boolean;
  onClick?: () => void;
}

export const BackwardCard: React.FC<BackwardCardProps> = ({
  backwardCard,
  isFocused = false,
  onClick,
}) => {
  return (
    <S.Card isFocused={isFocused} onClick={() => onClick?.()}>
      <S.CardWrapper>
        <S.BlackStripe />

        <S.CvvContainer>
          <S.CardCvvInput />
        </S.CvvContainer>
      </S.CardWrapper>
    </S.Card>
  );
};
