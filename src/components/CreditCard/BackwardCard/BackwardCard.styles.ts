import styled from "styled-components";

interface CardProps {
  isFocused: boolean;
}

export const Card = styled.article<CardProps>`
  position: relative;

  width: 100%;
  max-width: 350px;
  aspect-ratio: 350 / 220;

  box-shadow: ${(props) => (props.isFocused ? 60 : 12)}px 12px 8px #00000033;
  transform: scale(${(props) => (props.isFocused ? 1.105 : 1)});
  transition: all 0.4s;
`;

export const CardWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;

  display: flex;
  flex-direction: column;
  gap: 12px;

  width: 100%;
  height: 100%;
  padding: 20px 0;

  background: linear-gradient(
    68.44deg,
    #280537 0%,
    #56034c 18.54%,
    #890058 41.98%,
    #bc005b 66.98%,
    #eb1254 100%
  );

  border-radius: 12px;

  transition: all 2s;
`;

export const BlackStripe = styled.div`
  width: 100%;
  height: 40px;

  background: linear-gradient(
    119.14deg,
    rgba(0, 0, 0, 0.8) 10.46%,
    rgba(0, 0, 0, 0.7) 91.75%
  ); ;
`;

export const CvvContainer = styled.div`
  margin-left: 18px;
  width: 75%;

  background-color: #ffffff33;

  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

const CardInput = styled.input.attrs({
  required: true,
})`
  width: 100%;

  background: transparent;

  font-weight: 500;
  color: #2a2a2a;

  &::placeholder {
    color: #ffffff66;
  }
`;

export const CardCvvInput = styled(CardInput)`
  width: 52px;
  background-color: #ffffff66;

  padding: 4px 10px;

  text-align: center;

  font-size: 14px;
  font-weight: 700;
  font-weight: 28px;
`;
