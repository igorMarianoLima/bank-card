import styled from "styled-components";

export const CardContainer = styled.div`
  width: 100%;

  display: flex;
  justify-content: center;
  align-items: center;
  gap: 48px;
`;
