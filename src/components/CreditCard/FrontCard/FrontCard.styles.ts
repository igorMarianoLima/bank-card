import styled from "styled-components";

interface CardProps {
  isFocused: boolean;
}

export const Card = styled.article<CardProps>`
  position: relative;
  z-index: 1;

  width: 100%;
  max-width: 350px;
  aspect-ratio: 350 / 220;

  box-shadow: ${(props) => (props.isFocused ? 60 : 12)}px 12px 8px #00000033;
  transform: scale(${(props) => (props.isFocused ? 1.105 : 1)});
  transition: all 0.4s;
`;

export const CardWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;

  width: 100%;
  height: 100%;
  padding: 20px 18px;

  background: linear-gradient(
    68.44deg,
    #280537 0%,
    #56034c 18.54%,
    #890058 41.98%,
    #bc005b 66.98%,
    #eb1254 100%
  );

  border-radius: 12px;

  transition: all 2s;
`;

export const CardContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  position: relative;

  width: 100%;
  height: 100%;
`;

export const CardBrandWrapper = styled.div`
  display: flex;
  align-items: center;

  width: 100%;
  height: 44px;
`;

export const ContactLessWrapper = styled.div`
  position: absolute;
  top: 50%;
  right: 0;

  display: flex;
  align-items: center;
  justify-content: flex-end;

  width: 19px;
  height: 24px;
`;

export const ChipWrapper = styled.div`
  position: absolute;
  bottom: 0;
  right: 0;

  width: 35px;
  height: 27px;

  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

export const CardInformations = styled.div`
  display: flex;
  flex-direction: column;
  gap: 20px;

  width: 100%;
  max-width: calc(100% - 2 * 18px - 35px);
`;

export const CardSubInformations = styled(CardInformations)`
  max-width: unset;

  flex-direction: row;
`;

const CardInput = styled.input.attrs({
  required: true,
})`
  width: 100%;

  background: transparent;

  font-weight: 500;
  color: #fff;

  &::placeholder {
    color: #ffffff66;
  }

  &:not(:valid) {
    border-bottom: 1px solid #ffffff1a;
  }
`;

export const CardNumberInput = styled(CardInput)`
  font-size: 18px;
  font-weight: 28px;
`;

export const CardNumberHolder = styled(CardInput)`
  font-size: 12px;
  font-weight: 18px;
`;

export const CardNumberExpiration = styled(CardNumberHolder)``;
