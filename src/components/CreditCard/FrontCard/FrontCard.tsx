import React, { useState, useEffect } from "react";

import { Visa } from "../../../assets/svg/CardBrands/Visa";
import { Mastercard } from "../../../assets/svg/CardBrands/Mastercard";
import { Amex } from "../../../assets/svg/CardBrands/Amex";
import { Discover } from "../../../assets/svg/CardBrands/Discover";

import { ContactLess } from "../../../assets/svg/CardContactLess";
import { CardChip } from "../../../assets/svg/CardChip";

import * as S from "./FrontCard.styles";

type CardBrand = "VISA" | "MASTERCARD" | "AMEX" | "DISCOVER";

export interface CardFrontProps {
  cardNumber: string;
  cardHolder: string;
  cardExpirationDate: string;
  brand?: CardBrand;
}

interface FrontCardProps {
  frontalCard: CardFrontProps;
  isFocused?: boolean;
  onClick?: () => void;
}

type CardBrands = Record<CardBrand, RegExp>;

export const FrontCard: React.FC<FrontCardProps> = ({
  frontalCard,
  isFocused = false,
  onClick,
}) => {
  const [cardInformations, setCardInformations] = useState<CardFrontProps>({
    ...frontalCard,
  });

  const CARD_BRAND: Record<CardBrand, React.ReactElement> = {
    VISA: <Visa />,
    MASTERCARD: <Mastercard />,
    AMEX: <Amex />,
    DISCOVER: <Discover />,
  };

  const getCardBrand = (cardNumber: string) => {
    const brandsRegex: CardBrands = {
      VISA: /^4[0-9]{12}(?:[0-9]{3})?$/,
      MASTERCARD: /^5[1-5][0-9]{14}$/,
      AMEX: /^3[47][0-9]{13}$/,
      DISCOVER: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
    };

    for (const brand in brandsRegex) {
      const actualRegexIndex = brand as CardBrand;
      const actualRegex = brandsRegex[actualRegexIndex];

      const isValid = actualRegex.test(cardNumber.replace(/ /g, ""));

      if (isValid) {
        return setCardInformations((prev) => ({
          ...prev,
          brand: actualRegexIndex,
        }));
      }
    }

    setCardInformations((prev) => ({
      ...prev,
      brand: undefined,
    }));
  };

  const handleCardFill = (field: keyof CardFrontProps, newValue: string) => {
    setCardInformations((prev) => ({
      ...prev,
      [field]: newValue,
    }));
  };

  const isAllFilled = () => {
    const { cardExpirationDate, cardHolder, cardNumber } = cardInformations;

    return cardExpirationDate.trim() && cardHolder.trim() && cardNumber.trim();
  };

  useEffect(() => {
    getCardBrand(cardInformations.cardNumber);
  }, [cardInformations.cardNumber]);

  return (
    <S.Card isFocused={isFocused} onClick={() => onClick?.()}>
      <S.CardWrapper>
        <S.CardContainer>
          <S.CardBrandWrapper>
            {cardInformations["brand"] && CARD_BRAND[cardInformations["brand"]]}
          </S.CardBrandWrapper>

          <S.ContactLessWrapper>
            <ContactLess />
          </S.ContactLessWrapper>

          <S.ChipWrapper>
            <CardChip />
          </S.ChipWrapper>

          <S.CardInformations>
            <S.CardNumberInput
              placeholder={"0000 0000 0000 0000"}
              value={cardInformations.cardNumber}
              onChange={(ev) => handleCardFill("cardNumber", ev.target.value)}
            />

            <S.CardSubInformations>
              <S.CardNumberHolder
                placeholder={"John Doe"}
                value={cardInformations.cardHolder}
                onChange={(ev) => handleCardFill("cardHolder", ev.target.value)}
              />
              <S.CardNumberExpiration
                placeholder={"01/01"}
                value={cardInformations.cardExpirationDate}
                onChange={(ev) =>
                  handleCardFill("cardExpirationDate", ev.target.value)
                }
              />
            </S.CardSubInformations>
          </S.CardInformations>
        </S.CardContainer>
      </S.CardWrapper>
    </S.Card>
  );
};
