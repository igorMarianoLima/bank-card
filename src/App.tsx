import GlobalStyle, { Main } from "./styles/GlobalStyle";

import { CreditCard } from "./components/CreditCard/CreditCard";

function App() {
  return (
    <>
      <GlobalStyle />
      <Main>
        <CreditCard
          creditCard={{
            cardExpirationDate: "",
            cardHolder: "",
            cardNumber: "",
            cvv: "",
          }}
        />
      </Main>
    </>
  );
}

export default App;
