import styled, { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;

        box-sizing: border-box;

        text-decoration: none;
        list-style: none;

        font-family: sans-serif;

        border: 0;
        outline: 0;
    }

    button, input[type="submit"] {
        cursor: pointer;
    }
`;

export const Main = styled.main`
  width: 100%;
  height: 100vh;

  display: flex;
  align-items: center;
  justify-content: center;

  background: linear-gradient(
    119.14deg,
    rgba(0, 0, 0, 0.84) 10.46%,
    rgba(0, 0, 0, 0.95) 91.75%
  );
`;

export default GlobalStyle;
